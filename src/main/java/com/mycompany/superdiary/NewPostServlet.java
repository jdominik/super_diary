/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.superdiary;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Optional;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author RENT
 */
public class NewPostServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        

           
        request.getRequestDispatcher("/index.do").forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        SessionFactory instance = ConfigHibernate.getInstance();
        Session openSession = instance.openSession();
        Transaction beginTransaction = openSession.beginTransaction();
        Post post = new Post();
        User user = new User();
        Query createQuery = openSession.createQuery("FROM User WHERE name='" + request.getParameter("username") + "' AND pass= '" + request.getParameter("pass") + "'");

        User result = (User) createQuery.uniqueResult();
        beginTransaction.commit();
        //openSession.close();

        if (result != null) {
            Session openSession2 = instance.openSession();
            Transaction beginTransaction2 = openSession2.beginTransaction();
            //beginTransaction2.rollback();
            post.setTitle(request.getParameter("title"));
            post.setContent(request.getParameter("content"));
            post.setUserid(result);
            openSession.save(user);
            //beginTransaction2.commit();
            //openSession.close();
            processRequest(request, response);  
            
        } else {
            try (PrintWriter out = response.getWriter()) {
                /* TODO output your page here. You may use following sample code. */
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet PostServlet</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Wrong identity data!</h1>");
                out.println("</body>");
                out.println("</html>");
            //request.getRequestDispatcher("/index.do").forward(request, response);
            //processRequest(request, response);
        }
        
    }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

